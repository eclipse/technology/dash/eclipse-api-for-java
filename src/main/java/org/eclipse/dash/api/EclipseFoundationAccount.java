/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import jakarta.json.JsonObject;

public class EclipseFoundationAccount {
	JsonObject data;
	
	public EclipseFoundationAccount(JsonObject data) {
		this.data = data;
	}
	
	public boolean exists() {
		return !data.isEmpty();
	}
	
	public boolean isCommitter() {
		return data.getBoolean("is_committer", false);
	}

	public int getOrganizationId() {
		return data.getInt("org_id", -1);
	}

	String getProjectsUrl() {
		return data.getString("projects_url", null);
	}
}