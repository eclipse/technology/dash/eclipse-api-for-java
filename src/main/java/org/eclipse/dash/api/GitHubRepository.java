/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.json.JsonObject;

/**
 * Experimental. This is too tightly coupled with the GitHub API.
 * This currently represents the "just make it work" stage of the
 * experiment.
 */
public class GitHubRepository implements SourceRepository {
	JsonObject data;
	private String path;
	private String name;

	public GitHubRepository(JsonObject data) {
		this.data = data;

		Pattern pattern = Pattern.compile("https?:\\/\\/github\\.com\\/(?<path>[^\\/]+)\\/(?<name>[^\\/]+)");
		Matcher matcher = pattern.matcher(data.getString("url"));
		if (matcher.matches()) {
			this.path = matcher.group("path");
			this.name = matcher.group("name");
		}
	}

	@Override
	public boolean isMetadata() {
		return ".github".equals(getName());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public String getUrl() {
		return data.getString("url", null);
	}
	
	@Override
	public String toString() {
		return getUrl();
	}
}
