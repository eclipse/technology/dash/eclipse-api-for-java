/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jakarta.json.JsonObject;
import jakarta.json.JsonValue;

public class Release implements Comparable<Release>{

	private JsonObject data;

	public Release(JsonObject each) {
		this.data = each;
	}
	
	public boolean exists() {
		return !data.isEmpty();
	}
	
	public String getName() {
		return data.getString("title", null);
	}
	
	public Date getDate() {
		String value = data.getString("date", null);
		if (value == null) return null;
		
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(value);
		} catch (ParseException e) {
			// TODO Log this (debug)
			return null;
		}
	}

	@Override
	public int compareTo(Release other) {
		return getDate().compareTo(other.getDate());
	}
	
	@Override
	public String toString() {
		return getName() + " on " + getDate();
	}
}