/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.util.stream.Stream;

import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;

public class RepositoriesApi {

	EclipseApi eclipseApi;

	public RepositoriesApi(EclipseApi eclipseApi) {
		this.eclipseApi = eclipseApi;
	}
	
	public Stream<SourceRepository> githubRepositories(Project project) {
		return project.apiData.getOrDefault("github_repos", JsonValue.EMPTY_JSON_ARRAY).asJsonArray().stream()
				.map(each -> new GitHubRepository(each.asJsonObject()));
	}

	/**
	 * Get GitLab information for the project.
	 * 
	 * A project's single GitLab project group is specified in the
	 * <code>project_group</code> property in the <code>gitlab</code> section of the
	 * project's API data. GitLab projects in that group and in all subgroups are
	 * considered to be owned by the project. The section may also include a
	 * <code>ignored_sub_groups</code> section which lists subgroups that contain
	 * GitLab projects that contain content that is not considered to be project
	 * content.
	 * 
	 * <pre>
	 * ...
	 * "gitlab" : {
	 *   "ignored_sub_groups" : [
	 *     "eclipse/technology/dash/sync-script-testing/exclude-test"
	 *   ],
	 *   "project_group" : "eclipse/technology/dash"
	 * },
	 * "gitlab_repos" : [
	 *   {
	 *     "url" : "https://gitlab.eclipse.org/eclipse/dash/org.eclipse.dash.handbook"
	 *   }
	 * ],
	 * ...
	 * </pre>
	 * 
	 * Note that the <code>gitlab_repos</code> values are old data that are ignored.
	 *
	 * @param project
	 * @return
	 */
	public String gitlabProjectGroup(Project project) {
		return gitlab(project).getString("project_group", null);
	}
	
	public Stream<String> getIgnoredGitlabSubgroups(Project project) {
		return gitlab(project).getOrDefault("ignored_sub_groups", JsonValue.EMPTY_JSON_ARRAY).asJsonArray().stream()
				.filter(each -> each.getValueType() == JsonValue.ValueType.STRING)
				.map(each -> ((JsonString)each).getString());
	}

	private JsonObject gitlab(Project project) {
		return project.apiData.getOrDefault("gitlab", JsonValue.EMPTY_JSON_OBJECT).asJsonObject();
	}

	public String getGitHubOrg(Project project) {
		return project.apiData.getOrDefault("github", JsonValue.EMPTY_JSON_OBJECT).asJsonObject().getString("org", null);
	}
}
