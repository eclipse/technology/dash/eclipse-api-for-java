/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import jakarta.json.JsonObject;

public class Proposal {

	private JsonObject data;

	public Proposal(JsonObject data) {
		this.data = data;
	}

	public boolean exists() {
		return !data.isEmpty();
	}
	
	public String getName() {
		return data.getString("ProposalName", null);
	}
	
	public String getProjectId() {
		return data.getString("ProjectId", null);
	}
}
