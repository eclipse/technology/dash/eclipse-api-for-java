/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.util.Date;
import java.util.stream.Stream;

import jakarta.json.JsonObject;
import jakarta.json.JsonValue;

public class Project {
	JsonObject apiData;
	
	Project(JsonObject apiData) {
		this.apiData = apiData;
	}
	
	public boolean exists() {
		return !apiData.isEmpty();
	}
	
	public String getId() {
		return apiData.getString("project_id", null);
	}
	
	public String getName() {
		return apiData.getString("name", null);
	}

	public String getUrl() {
		return apiData.getString("url", null);
	}
	
	public Stream<Release> getReleases() {
		return apiData.getOrDefault("releases", JsonValue.EMPTY_JSON_ARRAY).asJsonArray().stream().map(each -> new Release(each.asJsonObject()));
	}
	
	public Release getNextRelease() {
		var now = new Date(System.currentTimeMillis());
		return getReleases().sorted().dropWhile(each -> each.getDate().before(now)).findFirst().orElseGet(() -> new Release(JsonValue.EMPTY_JSON_OBJECT));
	}

	public Stream<String> getProjectLeadIds() {
		return apiData.getOrDefault("project_leads", JsonValue.EMPTY_JSON_ARRAY).asJsonArray().stream().map(each -> each.asJsonObject().getString("username"));
	}
	
	public Stream<String> getCommitterIds() {
		return apiData.getOrDefault("committers", JsonValue.EMPTY_JSON_ARRAY).asJsonArray().stream().map(each -> each.asJsonObject().getString("username"));
	}
	
	/**
	 * Answers whether or not the project has a committer, as identified by
	 * committer id.
	 * 
	 * e.g.,
	 * 
	 * <pre>
	 * project.hasCommitter("wbeaton")
	 * </pre>
	 * 
	 * @param id A committer id as specified in the foundation DB.
	 * @return <code>true</code> when the project has an active committer with the
	 *         specified id, or <code>false</code> otherwise.
	 */
	public boolean hasCommitter(String id) {
		return getCommitterIds().anyMatch(each -> each.equals(id));
	}
}