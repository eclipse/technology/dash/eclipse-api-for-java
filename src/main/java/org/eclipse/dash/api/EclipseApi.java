/*************************************************************************
 * Copyright (c) 2021 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.JsonStructure;
import jakarta.json.JsonValue;
import jakarta.json.JsonValue.ValueType;

public class EclipseApi {
	final Logger logger = LoggerFactory.getLogger(EclipseApi.class);
	HttpService httpService;

	private String baseUrl = "https://api.eclipse.org";
	private String baseProjectUrl = "https://projects.eclipse.org";
	
	public EclipseApi() {
		this(new HttpService() {});
	}
	
	public EclipseApi(HttpService httpService) {
		this.httpService = httpService;
	}

	public EclipseApi setBaseUrl(String url) {
		baseUrl = url;
		return this;
	}

	public EclipseApi setBaseProjectUrl(String url) {
		baseProjectUrl = url;
		return this;
	}
	
	public RepositoriesApi getRepositoriesApi() {
		return new RepositoriesApi(this);
	}
	
	public EclipseFoundationAccount getAccount(String name) {
		// FIXME sanitize input.
		var data = new AtomicReference<JsonObject>();
		
		logger.debug("Querying account information for {}", name);
		
		String url = getAccountProfileUrl(name);
		var result = httpService.get(url, "application/json", response -> {
			JsonReader reader = Json.createReader(new InputStreamReader(response));
			data.set((JsonObject) reader.read());		
		});
		
		if (result == 404) {
			logger.info("No user with ID {} found (404)", name);
			return new EclipseFoundationAccount(JsonValue.EMPTY_JSON_OBJECT);			
		}
		
		if (result != 200 || data.get() == null) {
			throw new EclipseApiException("An error occurred while calling the API.");	
		};
		
		return new EclipseFoundationAccount(data.get());
		
	}
	
	public Bot getBot(String username) {
		// FIXME We should be able to share some implementation
		// FIXME sanitize input
		var data = new AtomicReference<JsonStructure>(JsonValue.EMPTY_JSON_ARRAY);
		
		logger.debug("Querying bot information for {}", username);
		
		String url = getBotUrl(username);
		var result = httpService.get(url, "application/json", response -> {
			JsonReader reader = Json.createReader(new InputStreamReader(response));
			data.set((JsonStructure) reader.read());
		});
		
		if (result == 404 || data.get() == null) {
			logger.info("No bot named {} found (404)", username);
			return new Bot(JsonValue.EMPTY_JSON_ARRAY);			
		}
		
		if (result != 200) {
			throw new RuntimeException(this.getClass().getName() + ": An error occurred while calling the API.");

		};
		
		return new Bot(data.get().asJsonArray());
	}

	/**
	 * Answer information about an Eclipse project specified by id (e.g.,
	 * "technology.dash"). This method always answers a value; use the
	 * {@link Project#exists()} method to test to whether or not the returned value
	 * represents a valid project,
	 * 
	 * @param id a project id (e.g., "technology.dash").
	 * @return an instance of {@link Project}
	 */
	public Project getProject(String id) {
		return new Project(getApiData(id));
	}
	
	public Proposal getProposal(String url) {
		return new Proposal(getProposalData(url));
	}
	
	private JsonObject getProposalData(String url) {
		// FIXME This is super expensive for what it does. Refactor.
		// The current implementation queries for all proposals,
		// returns one that matches, and throws everything else away.
		if (url == null) return JsonValue.EMPTY_JSON_OBJECT;
		
		var data = new AtomicReference<JsonObject>(JsonValue.EMPTY_JSON_OBJECT);
		var proposalsUrl = "https://projects.eclipse.org/json/proposals/all";
		var result = httpService.get(proposalsUrl, "application/json", response -> {
			JsonReader reader = Json.createReader(new InputStreamReader(response));
			JsonArray proposals = reader.readArray();
			proposals.stream()
				.filter(each -> url.equals(each.asJsonObject().getString("ProposalURL", null)))
				.findAny()
				.ifPresent(found -> data.set(found.asJsonObject()));
		});
		
		if (result != 200 || data.get() == null) {
			throw new RuntimeException(this.getClass().getName() + ": An error occurred while calling the API.");
		};
		
		if (data.get().getValueType() != ValueType.OBJECT) 
			return JsonValue.EMPTY_JSON_OBJECT;
		
		return data.get();
	}
	
	private JsonObject getApiData(String id) {
		if (id == null) return JsonValue.EMPTY_JSON_OBJECT;
		
		var url = getBaseProjectUrl() + "/api/projects/" + id.replace('.', '_');
		
		var data = new AtomicReference<JsonStructure>(JsonValue.EMPTY_JSON_OBJECT);

		var result = httpService.get(url, "application/json", response -> {
			JsonReader reader = Json.createReader(new InputStreamReader(response));
			data.set((JsonStructure) reader.read());
		});
		
		if (result == 400 || result == 404) {
			// FIXME As of December 2024, we get a 400 (Bad Request) error when we ask
			// for a project that does not exist. There may be other things that cause a
			// error 400; for now, though, we assume that a 400 means "Not Found".
			// See https://gitlab.eclipse.org/eclipsefdn/it/websites/projects.eclipse.org/-/issues/381
			return JsonValue.EMPTY_JSON_OBJECT;
		}

		if (result != 200 || data.get() == null) {
			throw new EclipseApiException("An error occurred while calling the API.");
		};
		
		if (data.get().getValueType() != ValueType.ARRAY) {
			return JsonValue.EMPTY_JSON_OBJECT;
		}
		
		JsonArray projects = data.get().asJsonArray();
		if (projects.isEmpty()) {
			return JsonValue.EMPTY_JSON_OBJECT;
		}
		
		return projects.get(0).asJsonObject();
	}
	
	public Collection<ProjectRoles> getProjects(EclipseFoundationAccount account) {
		var url = account.getProjectsUrl();
		
		var data = new AtomicReference<JsonStructure>(JsonValue.EMPTY_JSON_OBJECT);
		
		var result = httpService.get(url, "application/json", response -> {
			JsonReader reader = Json.createReader(new InputStreamReader(response));
			data.set((JsonStructure) reader.read());
		});
		
		if (result != 200 || data.get() == null) {
			throw new EclipseApiException("An error occurred while calling the API.");
		};
		
		return data.get().asJsonObject().entrySet().stream()
				.map(entry -> new ProjectRoles(entry.getKey(), entry.getValue().asJsonArray()))
				.filter(each -> !each.getProject().startsWith("foundation-internal."))
				.collect(Collectors.toSet());
	}

	private String getAccountProfileUrl(String id) {
		return getBaseUrl() + "/account/profile/" + id;
	}
	
	private String getBotUrl(String email) {
		return getBaseUrl() + "/bots?q=" + email;
	}

	private String getBaseUrl() {
		return baseUrl;
	}
	
	private String getBaseProjectUrl() {
		return baseProjectUrl;
	}
	
	/**
	 * The HttpService interface bottlenecks the means by which we actually
	 * make the calls to the API. By defining it in this manner, we can easily
	 * override how we make the actual call. This is handy when we're testing,
	 * or when we otherwise want/need to set up the calls in a different manner.
	 * 
	 * TODO investigate using Jakarta RESTful Web Services.
	 */
	public interface HttpService {
		default public int get(String url, String contentType, Consumer<InputStream> handler) {
			try {
				HttpRequest request = HttpRequest
						.newBuilder(URI.create(url))
						.header("Content-Type", contentType)
						.GET()
						.build();
				
				HttpClient httpClient = HttpClient.newBuilder()
						.followRedirects(Redirect.NORMAL)
						.build();
				
				HttpResponse<InputStream> response = httpClient.send(request, BodyHandlers.ofInputStream());
				
				if (response.statusCode() == 200) {
					handler.accept(response.body());
				}
				return response.statusCode();
			} catch (IOException | InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
