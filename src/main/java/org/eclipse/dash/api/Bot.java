/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;

public class Bot {
	JsonArray data;
	
	public Bot(JsonArray data) {
		this.data = data;
	}


	public String getUsername() {
		return getRootObject().getString("username", null);
	}
	
	public String getProjectId() {
		return getRootObject().getString("projectId", null);
	}

	private JsonObject getRootObject() {
		if (data == null) return JsonValue.EMPTY_JSON_OBJECT;
		if (data.isEmpty()) return JsonValue.EMPTY_JSON_OBJECT;
		
		return data.asJsonArray().get(0).asJsonObject();
	}


	public boolean exists() {
		return getRootObject().getInt("id", -1) > 0;
	}
}