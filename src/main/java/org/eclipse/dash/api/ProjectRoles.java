/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import java.util.Collection;
import java.util.stream.Collectors;

import jakarta.json.JsonArray;

public class ProjectRoles {

	private String project;
	private JsonArray roles;

	public ProjectRoles(String project, JsonArray roles) {
		this.project = project;
		this.roles = roles;
	}

	public String getProject() {
		return project;
	}
	
	public Collection<ProjectRole> getRoles() {
		return roles.stream().map(each -> new ProjectRole(project, each.asJsonObject())).collect(Collectors.toSet());
	}

}
