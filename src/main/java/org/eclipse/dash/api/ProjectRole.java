/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api;

import jakarta.json.JsonObject;
import jakarta.json.JsonValue;

public class ProjectRole {

	private String project;
	private JsonObject data;

	public ProjectRole(String project, JsonObject data) {
		this.project = project;
		this.data = data;
		// TODO Auto-generated constructor stub
	}

	public boolean isCommitter() {
		return "CM".equals(getRelationCode());
	}
	
	String getRelationCode() {
		return data.getOrDefault("Relation", JsonValue.EMPTY_JSON_OBJECT).asJsonObject()
				.getString("Relation", null);
	}

	public String getProject() {
		return project;
	}

}
