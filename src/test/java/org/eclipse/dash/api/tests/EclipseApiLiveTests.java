/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.RepositoriesApi;
import org.junit.jupiter.api.Test;

/**
 * These are "live" tests of the API. That is, the tests are executed against
 * the live API server. There is some risk that these tests will break due to
 * changes in the data provided by the service.
 */
class EclipseApiLiveTests {
	
	private EclipseApi getEclipseApi() {
		return new EclipseApi();
	}

	@Test
	void testBotExists() {
		var bot = getEclipseApi().getBot("eclipselink-bot");
		
		assertTrue(bot.exists());
		assertEquals("genie.eclipselink", bot.getUsername());
		assertEquals("ee4j.eclipselink", bot.getProjectId());
	}
	
	@Test
	void testBotDoesNotExist() {
		var bot = getEclipseApi().getBot("bogus-bot@eclipse.org");
		
		assertFalse(bot.exists());
	}
	
	@Test
	void testNullBot() {
		var bot = getEclipseApi().getBot(null);
		
		assertFalse(bot.exists());
	}
	
	@Test
	void testAccountIsCommitter() {
		var account = getEclipseApi().getAccount("wbeaton");
		
		assertTrue(account.exists());
		assertTrue(account.isCommitter());
		assertEquals(854, account.getOrganizationId());
	}
	
	@Test
	void testAccountIsNotCommitter() {
		// FYI, an account with ID "bogus" actually exists.
		var account = getEclipseApi().getAccount("bogus");
		
		assertFalse(account.isCommitter());
	}
	
	@Test
	void testAccountDoesNotExist() {
		// Since we're going against live data that will change, I've tried to
		// pick a name that is unlikely to occur in nature. Johnny Marr really
		// doesn't need Morrissey. Seriously, look up is solo rendition of "How
		// Soon is Now". Chills.
		var account = getEclipseApi().getAccount("JohnnyMarrDoesntNeedMorrissey");
		
		assertFalse(account.exists());
	}
	
	@Test
	void testGetCommitterProjects() {
		var account = getEclipseApi().getAccount("wbeaton");
		var projects = getEclipseApi().getProjects(account);
		
		var roles = projects.stream().flatMap(each -> each.getRoles().stream())
				.filter(role -> role.isCommitter()).map(each-> each.getProject())
				.collect(Collectors.toSet());
		assertTrue(roles.contains("technology.dash"));
	}
	
	@Test
	void testProject1() {
		var project = getEclipseApi().getProject("eclipse");
		
		assertTrue(project.exists());
		assertEquals("Eclipse Project", project.getName());
	}
	
	@Test
	void testProject2() {
		var project = getEclipseApi().getProject("technology.dash");
		
		assertTrue(project.exists());
		assertEquals("Eclipse Dash", project.getName());
	}
	
	@Test
	void testProject3() {
		var project = getEclipseApi().getProject("technology.escet");
		
		assertTrue(project.exists());
		assertEquals("Eclipse ESCET (Supervisory Control Engineering Toolkit)", project.getName());
	}
	
	@Test
	void testProject4() {
		var project = getEclipseApi().getProject("tools.oomph");
		
		assertTrue(project.exists());
		assertEquals(null, project.getNextRelease().getDate());
	}
	
	@Test
	void testBogusProject1() {
		assertFalse(getEclipseApi().getProject("blah").exists());
	}
	
	@Test
	void testBogusProject2() {
		assertFalse(getEclipseApi().getProject("foundation-internal.ig.openmobility").exists());
	}
	
	@Test
	void testGetProjectLeads() {
		var project = getEclipseApi().getProject("technology.dash");
		assertTrue(project.getProjectLeadIds().anyMatch(each -> each.equals("wbeaton")));
	}
	
	@Test
	void testGetCommitters() {
		var project = getEclipseApi().getProject("technology.dash");
		assertTrue(project.getCommitterIds().anyMatch(each -> each.equals("wbeaton")));
	}
	
	@Test 
	void testGitHubRepositories1() {
		EclipseApi eclipseApi = getEclipseApi();
		RepositoriesApi repoApi = eclipseApi.getRepositoriesApi();
		
		var project = eclipseApi.getProject("automotive.velocitas");
		var repository = repoApi.githubRepositories(project)
				.filter(each -> "vehicle-model-python".equals(each.getName()))
				.findFirst().get();

		assertEquals("vehicle-model-python", repository.getName());
		assertEquals("eclipse-velocitas", repository.getPath());
		assertFalse(repository.isMetadata());
		assertEquals("https://github.com/eclipse-velocitas/vehicle-model-python", repository.getUrl());
	}
	
	@Test 
	void testGitHubRepositories2() {
		EclipseApi eclipseApi = getEclipseApi();
		RepositoriesApi repoApi = eclipseApi.getRepositoriesApi();
		
		var project = eclipseApi.getProject("technology.dash");
		var repository = repoApi.githubRepositories(project)
				.filter(each -> "dash-licenses".equals(each.getName()))
				.findFirst().get();
		
		assertEquals("dash-licenses", repository.getName());
		assertEquals("eclipse-dash", repository.getPath());
		assertFalse(repository.isMetadata());
		assertEquals("https://github.com/eclipse-dash/dash-licenses", repository.getUrl());
	}
	
	@Test 
	void testGitHubRepositories3() {
		EclipseApi eclipseApi = getEclipseApi();
		RepositoriesApi repoApi = eclipseApi.getRepositoriesApi();
		
		var project = eclipseApi.getProject("automotive.uprotocol");
		
		assertEquals("eclipse-uprotocol", repoApi.getGitHubOrg(project));
	}
	
	@Test 
	void testGitLabRepositories() {
		EclipseApi eclipseApi = getEclipseApi();
		RepositoriesApi repoApi = eclipseApi.getRepositoriesApi();
		
		var project = eclipseApi.getProject("technology.dash");
		
		assertEquals("eclipse/technology/dash", repoApi.gitlabProjectGroup(project));
		
		repoApi.getIgnoredGitlabSubgroups(project).anyMatch(each -> "eclipse/technology/dash/sync-script-testing/exclude-test".equals(each));
	}
}
