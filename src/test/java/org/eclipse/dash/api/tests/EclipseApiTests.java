/*************************************************************************
 * Copyright (c) 2021,2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.api.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.eclipse.dash.api.EclipseApi;
import org.junit.jupiter.api.Test;

class EclipseApiTests {

	@Test
	void test() {
		var api = new EclipseApi(new EclipseApi.HttpService() {
			@Override
			public int get(String url, String contentType, Consumer<InputStream> handler) {
				handler.accept(new ByteArrayInputStream("[{\"id\":21,\"projectId\":\"ee4j.eclipselink\",\"username\":\"genie.eclipselink\",\"email\":\"eclipselink-bot@eclipse.org\",\"github.com\":{\"username\":\"eclipse-eclipselink-bot\",\"email\":\"eclipselink-bot@eclipse.org\"},\"oss.sonatype.org\":{\"username\":\"eclipselink-bot\",\"email\":\"eclipselink-bot@eclipse.org\"}}]".getBytes(StandardCharsets.UTF_8)));
				return 200;
			}
		});
		
		var bot = api.getBot("blah");
		
		assertEquals("ee4j.eclipselink", bot.getProjectId());
	}
	
	@Test
	void testProject() {		
		var api = new EclipseApi(new EclipseApi.HttpService() {
			@Override
			public int get(String url, String contentType, Consumer<InputStream> handler) {
				handler.accept(new ByteArrayInputStream("{\"projects\":{\"technology.dash\":{\"title\":\"Eclipse Dash™\",\"description\":[{\"value\":\"<p>Eclipse Dash&trade; is a place where the community itself collaborates on tools for community awareness and collaboration in support of our ultimate objective of committer quality and cooperation.</p>\\r\\n\\r\\n<p>This is very much an incubator for committer tools and other resources that could improve the overall&nbsp;Eclipse Foundation community experience.</p>\\r\\n\\r\\n<p>The Dash project provides a safe and open space to prototype these tools and test them, and if they stick, eventually rolling them into production on the eclipse.org or dev.eclipse.org website.</p>\\r\\n\",\"summary\":\"\",\"format\":\"full_html\",\"safe_value\":\"<p>Eclipse Dash™ is a place where the community itself collaborates on tools for community awareness and collaboration in support of our ultimate objective of committer quality and cooperation.</p>\\n<p>This is very much an incubator for committer tools and other resources that could improve the overall Eclipse Foundation community experience.</p>\\n<p>The Dash project provides a safe and open space to prototype these tools and test them, and if they stick, eventually rolling them into production on the eclipse.org or dev.eclipse.org website.</p>\\n\",\"safe_summary\":\"\"}],\"parent_project\":[{\"id\":\"technology\"}],\"bugzilla\":[],\"build_url\":[],\"dev_list\":{\"name\":\"dash-dev\",\"email\":\"dash-dev@eclipse.org\",\"url\":\"https://dev.eclipse.org/mailman/listinfo/dash-dev\"},\"documentation_url\":[],\"download_url\":[],\"gettingstarted_url\":[],\"id\":[{\"value\":\"technology.dash\",\"format\":null,\"safe_value\":\"technology.dash\"}],\"licenses\":[{\"name\":\"Eclipse Public License 2.0\",\"url\":\"http://www.eclipse.org/legal/epl-2.0\"}],\"mailing_lists\":[],\"other_links\":[],\"plan_url\":[{\"url\":\"http://www.eclipse.org/dash/project-info/plan.xml\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"http://www.eclipse.org/dash/project-info/plan.xml\"}],\"proposal_url\":[{\"url\":\"http://www.eclipse.org/proposals/dash/\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"http://www.eclipse.org/proposals/dash/\"}],\"tags\":[],\"website_url\":[{\"url\":\"http://www.eclipse.org/dash/\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"http://www.eclipse.org/dash/\"}],\"wiki_url\":[],\"scope\":[{\"value\":\"<p>Eclipse Dash&trade; is a place where the community itself can collaborate on tools for community awareness and collaboration in support of our ultimate objective of committer quality and cooperation.</p>\\r\\n\",\"format\":\"filtered_html\",\"safe_value\":\"<p>Eclipse Dash™ is a place where the community itself can collaborate on tools for community awareness and collaboration in support of our ultimate objective of committer quality and cooperation.</p>\"}],\"source_repo\":[{\"type\":\"github\",\"name\":\"dash-licenses\",\"path\":\"https://github.com/eclipse/dash-licenses\",\"url\":\"https://github.com/eclipse/dash-licenses\"},{\"type\":\"github\",\"name\":\"dash-website\",\"path\":\"https://github.com/eclipse/dash-website\",\"url\":\"https://github.com/eclipse/dash-website\"}],\"state\":[{\"value\":\"Incubating\"}],\"build_description\":[],\"build_doc\":[],\"build_technologies\":[],\"forums\":[{\"name\":\"eclipse.technology.dash\",\"description\":\"A place where the community itself can collaborate on tools for community awareness and collaboration in support of our ultimate objective of committer quality and cooperation.\",\"url\":\"http://www.eclipse.org/forums/eclipse.technology.dash\"}],\"logo\":[],\"techology_types\":[{\"tid\":\"24\"}],\"contrib_message\":[{\"value\":\"<p>Open issues against the issue trackers on the corresponding repositories listed below.</p>\\r\\n\",\"format\":\"full_html\",\"safe_value\":\"<p>Open issues against the issue trackers on the corresponding repositories listed below.</p>\\n\"}],\"downloads\":[],\"downloads_message\":[],\"marketplace\":[],\"update_sites\":[],\"related\":[],\"team_project_sets\":[],\"github_repos\":[{\"url\":\"https://github.com/eclipse/dash-licenses\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"https://github.com/eclipse/dash-licenses\"},{\"url\":\"https://github.com/eclipse/dash-website\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"https://github.com/eclipse/dash-website\"}],\"documentation\":[],\"working_group\":[],\"spec_working_group\":[],\"patent_license\":[],\"contributors\":[{\"uid\":\"13499\"}],\"gitlab_repos\":[{\"url\":\"https://gitlab.eclipse.org/eclipse/dash/org.eclipse.dash.handbook\",\"title\":null,\"attributes\":[],\"original_title\":null,\"original_url\":\"https://gitlab.eclipse.org/eclipse/dash/org.eclipse.dash.handbook\"}],\"website_repo\":[{\"value\":\"1\",\"revision_id\":\"1\"}],\"gl_excl_sub_groups\":[{\"value\":\"eclipse/technology/dash/sync-script-testing/exclude-test\",\"format\":null,\"safe_value\":\"eclipse/technology/dash/sync-script-testing/exclude-test\"}],\"gl_project_group\":[{\"value\":\"eclipse/technology/dash\",\"format\":null,\"safe_value\":\"eclipse/technology/dash\"}],\"url\":\"https://projects.eclipse.org/projects/technology.dash\",\"releases\":[{\"title\":\"1.0 Handbook\",\"description\":[{\"value\":\"<p>This record corresponds to&nbsp;the release review of the first official release of the Eclipse Foundation Handbook.</p>\\r\\n\",\"summary\":\"\",\"format\":\"full_html\",\"safe_value\":\"<p>This record corresponds to the release review of the first official release of the Eclipse Foundation Handbook.</p>\\n\",\"safe_summary\":\"\"}],\"apis\":[{\"value\":\"0\"}],\"architecture\":[],\"communities\":[],\"compatibility\":[],\"date\":[{\"value\":\"2021-10-06 00:00:00\",\"timezone\":\"America/New_York\",\"timezone_db\":\"America/New_York\",\"date_type\":\"datetime\"}],\"deliverables\":[],\"endoflife\":[],\"environment\":[],\"i18n\":[],\"milestones\":[],\"noncode\":[],\"noteworthy\":[],\"parent_project\":[{\"id\":\"technology.dash\"}],\"security\":[],\"standards\":[],\"subprojects\":[],\"themes\":[],\"usability\":[],\"type\":[{\"value\":\"2\"}],\"conf_ui_guidelines\":[{\"value\":\"not_verified\"}],\"screenshots\":[],\"url\":\"https://projects.eclipse.org/projects/technology.dash/releases/1.0-handbook\",\"review\":{\"title\":\"1.0 Handbook Release Review\",\"description\":[],\"end_date\":[{\"value\":\"2021-10-06 00:00:00\",\"timezone\":\"America/New_York\",\"timezone_db\":\"America/New_York\",\"date_type\":\"datetime\"}],\"links\":[{\"url\":\"https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/103\",\"title\":\"GitLab tracking issue\",\"attributes\":[],\"original_title\":\"GitLab tracking issue\",\"original_url\":\"https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/103\"}],\"project\":[{\"id\":\"technology.dash\"}],\"reference\":[{\"title\":\"1.0 Handbook\"}],\"state\":[{\"value\":\"success\"}],\"top_level\":[{\"id\":\"technology\"}],\"type\":[{\"value\":\"release\"}],\"url\":\"https://projects.eclipse.org/projects/technology.dash/reviews/1.0-handbook-release-review\"}}]}}}".getBytes(StandardCharsets.UTF_8)));
				return 200;
			}
		});
		
		var project = api.getProject("technology.dash");
		
		assertTrue(project.exists());
		assertEquals("Wed Oct 06 00:00:00 EDT 2021", project.getReleases().findFirst().get().getDate().toString());
	}
	
	@Test
	void testBogusProject() {
		var api = new EclipseApi(new EclipseApi.HttpService() {
			@Override
			public int get(String url, String contentType, Consumer<InputStream> handler) {
				handler.accept(new ByteArrayInputStream("[]".getBytes(StandardCharsets.UTF_8)));
				return 200;
			}
		});
		
		var project = api.getProject("blah");
		
		assertFalse(project.exists());
	}
}
