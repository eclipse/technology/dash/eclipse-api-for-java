# Eclipse API for Java

[![REUSE status](https://api.reuse.software/badge/gitlab.eclipse.org/eclipse/technology/dash/eclipse-api-for-java)](https://api.reuse.software/info/gitlab.eclipse.org/eclipse/technology/dash/eclipse-api-for-java)

The Eclipse API for Java provides a lightweight Java interface to the Eclipse API. 

This implementation is neither complete, nor is it particularly robust. In its current form, it provides exactly what we need and nothing more.

The implementation does not include any notion of authenticating with the server. As such it can only access those APIs that do not require authentication and it is subject to the throttling policy for unauthenticated clients.

## License

This project is distributed under the terms of the Eclipse Public License 2.0 (EPL-2.0).